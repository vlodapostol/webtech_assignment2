
const FIRST_NAME = "Vlad";
const LAST_NAME = "Apostol";
const GRUPA = "1090";

/**
 * Make the implementation here
 */

function initCaching() {
    var cache={
    }
    cache.about=0;
    cache.home=0;
    cache.contact=0;
   var object={
    pageAccessCounter(section){
        if('about'.localeCompare(section,'en',{sensitivity:'accent'})==0){
            cache.about++;
        }
        if(section==='contact'){
            cache.contact++;
        }
        if(!section){
            cache.home++;
        }
       },

    getCache(){
        return cache;
       }
   }
   return object;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

